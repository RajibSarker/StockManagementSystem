﻿using StockManagementSystem.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StockManagementSystem
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new categorySetupForm());
            //Application.Run(new companySetupForm());
            //Application.Run(new itemSetupForm());
            //Application.Run(new stockInForm());
            //Application.Run(new stockOutForm());
            Application.Run(new ProgressForm());
            //Application.Run(new searchAndViewItemsSummaryForm());
            //Application.Run(new viewBetweenTwoDatesReportForm());
        }
    }
}
