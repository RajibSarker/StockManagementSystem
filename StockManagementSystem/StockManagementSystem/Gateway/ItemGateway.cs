﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using StockManagementSystem.Models;

namespace StockManagementSystem.Gateway
{
    class ItemGateway
    {
        ConnectionClass connection;
        SqlCommand cmd;
        SqlDataReader reader;
        public int SaveItem(Item item)
        {
            int row = 0;
            connection = new ConnectionClass();
            string query = "Insert Into Items(Name,ReOrderLevel,CategoryId,CompanyId,UserId,CreatedDate) Values(@name,@reOrder,@categoryId,@companyId,@userId,@date)";

            try
            {
                cmd = new SqlCommand(query, connection.GetConnection());
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@name", item.ItemName);
                cmd.Parameters.AddWithValue("@reOrder", item.ReorderLevel);
                cmd.Parameters.AddWithValue("@categoryId", item.CategoryId);
                cmd.Parameters.AddWithValue("@companyId", item.CompanyId);
                cmd.Parameters.AddWithValue("@userId", item.UserId);
                cmd.Parameters.AddWithValue("@date", item.CreatedDate);
                row = cmd.ExecuteNonQuery();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
            finally
            {
                connection.GetClose();
            }
            return row;
        }
        public int UpdateItem(Item item)
        {
            int row = 0;
            connection = new ConnectionClass();
            string query = "Update Items Set Name=@name,ReOrderLevel=@reOrder,UserId=@userId,CreatedDate=@date Where Id=@id";
            try
            {
                cmd = new SqlCommand(query, connection.GetConnection());
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@id", item.Id);
                cmd.Parameters.AddWithValue("@name", item.ItemName);
                cmd.Parameters.AddWithValue("@reOrder", item.ReorderLevel);
                cmd.Parameters.AddWithValue("@userId", item.UserId);
                cmd.Parameters.AddWithValue("@date", item.CreatedDate);
                row = cmd.ExecuteNonQuery();
            }
            catch (Exception exception)
            {
                row = 0;
            }
            finally
            {
                connection.GetClose();
            }
            return row;
        }
        public List<Item> GetItems()
        {
            List<Item> items = new List<Item>();
            connection = new ConnectionClass();
            string query = "Select i.Id AS ItemId,i.Name AS ItemName,i.ReOrderLevel,i.CreatedDate,i.UserId,co.Id as CompanyId,co.Name as CompanyName,ca.Id as CategoryId,ca.Name as CategoryName From Items AS i INNER JOIN Companys as co ON i.CompanyId=co.Id INNER JOIN Categories as ca ON i.CategoryId=ca.Id";
            try
            {
                cmd = new SqlCommand(query, connection.GetConnection());
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Item item = new Item();
                    item.Id = (int)(reader["ItemId"]);
                    item.ItemName = reader["ItemName"].ToString();
                    item.ReorderLevel = Convert.ToInt32(reader["ReOrderLevel"]);
                    item.CategoryId = Convert.ToInt32(reader["CategoryId"]);
                    item.CompanyId = Convert.ToInt32(reader["CompanyId"]);
                    item.CreatedDate = Convert.ToDateTime(reader["CreatedDate"]);
                    item.CategoryName = reader["CategoryName"].ToString();
                    item.CompanyName = reader["CompanyName"].ToString();
                    item.UserId = Convert.ToInt32(reader["UserId"]);
                    items.Add(item);
                }
            }
            catch (Exception exception)
            {
                items = null;
            }
            finally
            {
                connection.GetClose();
            }
            return items;
        }
        public bool IsExistItem(Item item)
        {
            connection = new ConnectionClass();
            string query = "Select * From Items where Name=@name";
            try
            {
                cmd = new SqlCommand(query, connection.GetConnection());
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@name", item.ItemName);
                reader = cmd.ExecuteReader();
                string itemName = item.ItemName.ToLower();
                while (reader.Read())
                {
                    if(item.ActionType == "Update")
                    {
                        string name = reader["Name"].ToString().ToLower();
                        if (Convert.ToInt32(reader["Id"]) == item.Id && name == itemName)
                        {
                            return false;
                        }
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                connection.GetClose();
            }
            return false;
        }
        public List<Item> GetItemsByCategoryAndCompany(int categoryId, int companyId)
        {
            List<Item> items = new List<Item>();
            connection = new ConnectionClass();
            string query = "Select * From Items Where CategoryId=@category AND CompanyId=@company";
            try
            {
                cmd = new SqlCommand(query, connection.GetConnection());
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@category", categoryId);
                cmd.Parameters.AddWithValue("@company", companyId);
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Item item = new Item();
                    item.Id = Convert.ToInt32(reader["Id"]);
                    item.ItemName = reader["Name"].ToString();
                    items.Add(item);
                }
            }
            catch (Exception ex)
            {
                items = null;
            }
            finally
            {
                connection.GetClose();
            }
            return items;
        }
        public int GetItemReorderLevel(Item item)
        {
            Item particularItem = new Item();
            connection = new ConnectionClass();
            string query = "Select * From Items Where Id=@id";
            try
            {
                cmd = new SqlCommand(query, connection.GetConnection());
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@id", item.Id);
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    particularItem.ReorderLevel = Convert.ToInt32(reader["ReOrderLevel"]);
                }
            }
            catch (Exception exception)
            {
                particularItem.ReorderLevel = 0;
            }
            finally
            {
                connection.GetClose();
            }
            return particularItem.ReorderLevel;
        }
    }
}
