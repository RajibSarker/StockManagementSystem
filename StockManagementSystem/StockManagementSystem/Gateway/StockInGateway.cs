﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StockManagementSystem.Models;

namespace StockManagementSystem.Gateway
{
    class StockInGateway
    {
        ConnectionClass connection;
        SqlCommand cmd;
        SqlDataReader reader;
        public int SaveItemQuantityInStockIn(Item item)
        {
            int row = 0;
            connection = new ConnectionClass();
            string query = "Insert Into StockIn(ItemId,Quantity,UserId,CreatedDate) Values(@id,@quantity,@userId,@date)";
            try
            {
                cmd = new SqlCommand(query, connection.GetConnection());
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@id", item.Id);
                cmd.Parameters.AddWithValue("@quantity", item.StockInQuantity);
                cmd.Parameters.AddWithValue("@userId", item.UserId);
                cmd.Parameters.AddWithValue("@date", item.CreatedDate);
                row = cmd.ExecuteNonQuery();
            }
            catch(Exception ex)
            {
                row =0;
            }
            finally
            {
                connection.GetClose();
            }
            return row;
        }
        public int UpdateItemQuantityInStockIn(Item item)
        {
            int row = 0;
            connection = new ConnectionClass();
            string query = "Update StockIn Set Quantity=@quantity,UserId=@userId,CreatedDate=@date Where Id=@id";
            try
            {
                cmd = new SqlCommand(query, connection.GetConnection());
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@quantity", item.StockInQuantity);
                cmd.Parameters.AddWithValue("@id", item.StockInId);
                cmd.Parameters.AddWithValue("@userId", item.UserId);
                cmd.Parameters.AddWithValue("@date", item.CreatedDate);
                row = cmd.ExecuteNonQuery();
            }
            catch(Exception ex)
            {
                row = 0;
            }
            finally
            {
                connection.GetClose();
            }
            return row;
        }
        public List<Item> GetItemsInStockIn()
        {
            List<Item> items = new List<Item>();
            connection = new ConnectionClass();
            string query = "Select s.Id,s.ItemId,i.Name AS ItemName,i.ReOrderLevel, s.CreatedDate,s.Quantity,c.Id as CategoryId,c.Name as CategoryName,co.Id as CompanyId,co.Name as CompanyName From StockIn AS s INNER JOIN Items AS i ON s.ItemId=i.id INNER JOIN Categories as c ON i.CategoryId=c.Id INNER JOIN Companys as co ON i.CompanyId=co.Id Where s.CreatedDate Between @fromDate AND @toDate";
            //string query = "Select i.Name as ItemName,c.Name as CompanyName,ca.Name as CatagoryName,i.ReOrderLevel ,sum(s.Quantity) as Quantity From StockIn as s inner join Items as i on s.ItemId=i.Id Inner join Companys as c on i.CompanyId=c.Id INNER JOIN Categories as ca ON i.CategoryId=ca.Id where s.CreatedDate Between @fromDate and @toDate  group by i.Name,c.Name,ca.Name,i.ReOrderLevel";
            //string query = "Select  i.Id as ItemId, i.Name as ItemName,c.Id as CompanyId,c.Name as CompanyName,ca.Id as CategoryId,ca.Name as CategoryName,i.ReOrderLevel ,sum(s.Quantity) as Quantity From StockIn as s inner join Items as i on s.ItemId=i.Id Inner join Companys as c on i.CompanyId=c.Id INNER JOIN Categories as ca ON i.CategoryId=ca.Id where s.CreatedDate Between @fromDate and @toDate  group by i.Name,c.Name,ca.Name,i.ReOrderLevel,i.Id,c.Id,ca.Id";
            try
            {
                cmd = new SqlCommand(query, connection.GetConnection());
                cmd.Parameters.Clear();
                var fromDate = DateTime.Now.AddDays(-5).ToShortDateString();
                var toDate = DateTime.Now.ToShortDateString();
                cmd.Parameters.AddWithValue("@fromDate", fromDate);
                cmd.Parameters.AddWithValue("@toDate", toDate);
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Item item = new Item();
                    item.Id = Convert.ToInt32(reader["ItemId"]);
                    item.ItemName = reader["ItemName"].ToString();
                    item.ReorderLevel = Convert.ToInt32(reader["ReOrderLevel"]);
                    item.AvailableQuantity = Convert.ToInt32(reader["Quantity"]);
                    item.CreatedDate =Convert.ToDateTime( reader["CreatedDate"]);
                    item.CategoryId = Convert.ToInt32(reader["CategoryId"]);
                    item.CategoryName = reader["CategoryName"].ToString();
                    item.CompanyId = Convert.ToInt32(reader["CompanyId"]);
                    item.CompanyName = reader["CompanyName"].ToString();
                    item.StockInId = Convert.ToInt32(reader["Id"]);
                    items.Add(item);
                }
            }
            catch(Exception ex)
            {
                items = null;
            }
            finally
            {
                connection.GetClose();
            }
            return items;
        }
        public int GetItemAvailableQuantity(int id)
        {
            int quantity = 0;
            connection = new ConnectionClass();
            string query = "Select Quantity From StockIn Where ItemId=@id";
            try
            {
                cmd = new SqlCommand(query, connection.GetConnection());
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@id", id);
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    quantity +=Convert.ToInt32(reader["Quantity"]);
                }
            }
            catch(Exception ex)
            {
                quantity = 0;
            }
            finally
            {
                connection.GetClose();
            }
            return quantity;
        }
    }
}
