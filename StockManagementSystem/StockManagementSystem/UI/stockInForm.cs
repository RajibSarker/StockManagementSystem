﻿using StockManagementSystem.Manager;
using StockManagementSystem.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StockManagementSystem.UI
{
    public partial class stockInForm : Form
    {
        public stockInForm()
        {
            InitializeComponent();
        }
        Item item = new Item();
        ItemManager itemManager = new ItemManager();
        StockInManager stockInManager = new StockInManager();
        StockOutManager stockOutManager = new StockOutManager();
        CategoryManager categoryManager = new CategoryManager();
        CompanyManager companyManager = new CompanyManager();
        UserManager userManager = new UserManager();

        private void stockInForm_Load(object sender, EventArgs e)
        {
            try
            {
                LoadCompaniesComboBox();
                //List<Item> items = stockInManager.GetItemsInStockIn();
                //BindItemListGridView(items);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void BindItemListGridView(List<Item> items)
        {
            try
            {
                itemsListGirdView.Rows.Clear();
                int serialNo = 0;
                foreach (var item in items)
                {
                    serialNo++;
                    itemsListGirdView.Rows.Add(serialNo, item.Id, item.ItemName, item.CreatedDate.ToShortDateString(), item.AvailableQuantity, item.CategoryId, item.CategoryName, item.CompanyId, item.CompanyName, item.ReorderLevel, item.StockInId);
                }
                itemsListGirdView.Sort(itemsListGirdView.Columns[0], ListSortDirection.Descending);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void LoadCompaniesComboBox()
        {
            try
            {
                List<Company> companies = companyManager.GetCompaies();
                Company company = new Company();
                company.Id = 0;
                company.CompanyName = "--Select--";
                companies.Insert(0, company);
                companiesComboBox.DataSource = null;
                companiesComboBox.DataSource = companies;
                companiesComboBox.DisplayMember = "CompanyName";
                companiesComboBox.ValueMember = "Id";
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void companiesComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (companiesComboBox.SelectedIndex > 0)
                {
                    Company company = new Company();
                    company.Id =Convert.ToInt32( companiesComboBox.SelectedValue);
                    List<Category> categories = categoryManager.GetCategoriesByCompany(company);
                    if (categories.Count > 0)
                    {
                        Category category = new Category();
                        category.Id = 0;
                        category.CategoryName = "--Select--";
                        categories.Insert(0, category);
                        categoriesComboBox.DataSource = null;
                        categoriesComboBox.DataSource = categories;
                        categoriesComboBox.DisplayMember = "CategoryName";
                        categoriesComboBox.ValueMember = "Id";
                    }
                    else
                    {
                        List<string> data = new List<string>();
                        data.Add("No Data Found!");
                        categoriesComboBox.DataSource = data;
                    }
                }
                else
                {
                    categoriesComboBox.DataSource = null;
                    itemsComboBox.DataSource = null;
                }
            }
            catch(Exception exception)
            {
                MessageBox.Show(exception.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void categoriesComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (categoriesComboBox.SelectedIndex > 0)
                {
                    int categoryId = Convert.ToInt32(categoriesComboBox.SelectedValue);
                    int companyId = Convert.ToInt32(companiesComboBox.SelectedValue);
                    List<Item> items = itemManager.GetItemsByCaGetItemsByCategoryAndCompanytegory(categoryId,companyId);
                    if (items.Count > 0)
                    {
                        Item item = new Item();
                        item.Id = 0;
                        item.ItemName = "--Select--";
                        items.Insert(0, item);
                        itemsComboBox.DataSource = null;
                        itemsComboBox.DataSource = items;
                        itemsComboBox.DisplayMember = "ItemName";
                        itemsComboBox.ValueMember = "Id";
                    }
                    else
                    {
                        List<string> data = new List<string>();
                        data.Add("No Data Found!");
                        itemsComboBox.DataSource = data;
                    }
                }
                else
                {
                    itemsComboBox.DataSource = null; 
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void itemsComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (itemsComboBox.SelectedIndex > 0)
                {
                    item.Id =Convert.ToInt32(itemsComboBox.SelectedValue);
                    reorderLeveTextBox.Text = itemManager.GetItemReorderLevel(item).ToString();
                    int availableQuantity=itemManager.GetAvailableQuantity(item.Id);
                    availableQuantityTextBox.Text = availableQuantity.ToString();
                    List<Item> items = stockInManager.GetItemsInStockIn().Where(c => c.Id == item.Id).ToList();
                    if (items.Count > 0)
                    {
                        BindItemListGridView(items);
                    }
                }
                else
                {
                    reorderLeveTextBox.Clear();
                    availableQuantityTextBox.Clear();
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (SaveButton.Text == "&Save")
                {
                    if (IsFormValid())
                    {
                        item.Id = Convert.ToInt32(itemsComboBox.SelectedValue);
                        item.StockInQuantity = Convert.ToInt32(stockInQuantityTextBox.Text);
                        item.UserId = userManager.GetUserId(LoginForm.UserName,LoginForm.Password);
                        item.CreatedDate = DateTime.Now;

                        int isSaved= stockInManager.SaveItemQuantityInStockIn(item);
                        if (isSaved > 0)
                        {
                            MessageBox.Show("Stock In Successful", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            var items = stockInManager.GetItemsInStockIn().Where(c=>c.Id==item.Id).ToList();
                            BindItemListGridView(items);
                            Reset();
                        }
                        else
                        {
                            MessageBox.Show("Stock In Failed!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
                if (SaveButton.Text == "Update")
                {
                    if (IsFormValid())
                    {
                        item.StockInId = Convert.ToInt32(idLabel.Text);
                        item.StockInQuantity = Convert.ToInt32(stockInQuantityTextBox.Text);
                        item.UserId = item.UserId = userManager.GetUserId(LoginForm.UserName, LoginForm.Password); ;
                        item.CreatedDate = DateTime.Now;
                        int isUpdated = stockInManager.UpdateItemQuantityInStockIn(item);
                        if (isUpdated > 0)
                        {
                            MessageBox.Show("Update Successful.", "Updated", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            var items = stockInManager.GetItemsInStockIn().Where(c=>c.Id==item.Id).ToList();
                            BindItemListGridView(items);
                            stockInQuantityLabel.Text = "";
                            CancelButton.Visible = false;
                            Reset();
                        }
                        else
                        {
                            MessageBox.Show("Update Failed!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Reset()
        {
            companiesComboBox.SelectedIndex = 0;
            reorderLeveTextBox.Text = "";
            availableQuantityTextBox.Text = "";
            stockInQuantityTextBox.Text = "";
            idLabel.Text = "";
            SaveButton.Text = "&Save";
            SaveButton.BackColor = Color.Indigo;
            companiesComboBox.Enabled = true;
            categoriesComboBox.Enabled = true;
            itemsComboBox.Enabled = true;
        }

        private bool IsFormValid()
        {
            if (companiesComboBox.SelectedIndex <= 0)
            {
                MessageBox.Show("Please select company name!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                companiesComboBox.Focus();
                return false;
            }
            if (categoriesComboBox.SelectedIndex <= 0)
            {
                MessageBox.Show("Please select category name!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                categoriesComboBox.Focus();
                return false;
            }
            if (itemsComboBox.SelectedIndex <= 0)
            {
                MessageBox.Show("Please select item name!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                itemsComboBox.Focus();
                return false;
            }
            if (stockInQuantityTextBox.Text.Trim()==string.Empty)
            {
                MessageBox.Show("Please enter stock in quantity!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                stockInQuantityTextBox.Clear();
                stockInQuantityTextBox.Focus();
                return false;
            }
            return true;
        }

        private void itemsListGirdView_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                DataGridViewRow dr = itemsListGirdView.SelectedRows[0];
                idLabel.Text = dr.Cells[10].Value.ToString();
                companiesComboBox.Text = dr.Cells[8].Value.ToString();
                categoriesComboBox.Text = dr.Cells[6].Value.ToString();
                itemsComboBox.Text = dr.Cells[2].Value.ToString();
                reorderLeveTextBox.Text = dr.Cells[9].Value.ToString();
                availableQuantityTextBox.Text = itemManager.GetAvailableQuantity(Convert.ToInt32(dr.Cells[1].Value)).ToString();
                stockInQuantityTextBox.Text = dr.Cells[4].Value.ToString();
                SaveButton.Text = "Update";
                SaveButton.BackColor = Color.Magenta;
                CancelButton.Visible = true;
                companiesComboBox.Enabled = false;
                categoriesComboBox.Enabled = false;
                itemsComboBox.Enabled = false;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void stockInQuantityTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (!char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                e.Handled = true;
            }
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            CancelButton.Visible = false;
            stockInQuantityLabel.Text = "";
            Reset();
        }
    }
}
