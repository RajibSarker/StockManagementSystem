﻿using StockManagementSystem.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StockManagementSystem.UI
{
    public partial class MasterForm : Form
    {
        public MasterForm()
        {
            InitializeComponent();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LoginForm loginForm = new LoginForm();
            loginForm.Show();
            this.Hide();
        }

        private void companySetupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            companySetupForm companySetupForm = new companySetupForm();
            companySetupForm.Show();
        }

        private void categorySetupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            categorySetupForm categorySetupForm = new categorySetupForm();
            categorySetupForm.Show();
        }

        private void iToolStripMenuItem_Click(object sender, EventArgs e)
        {
            itemSetupForm itemSetup = new itemSetupForm();
            itemSetup.Show();
        }

        private void stockInToolStripMenuItem_Click(object sender, EventArgs e)
        {
            stockInForm stockIN = new stockInForm();
            stockIN.Show();
        }

        private void MasterForm_Load(object sender, EventArgs e)
        {
            userLabel.Text = "";
            passwordLabel.Text = "";
            userLabel.Text ="User Name- "+ LoginForm.UserName;
            passwordLabel.Text = LoginForm.Password;
        }

        private void aboutToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            aboutForm about = new aboutForm();
            about.Show();
        }

        private void stockOutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            stockOutForm stockOut = new stockOutForm();
            stockOut.Show();
        }

        private void searchViewSummarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            searchAndViewItemsSummaryForm searchAndViewItems = new searchAndViewItemsSummaryForm();
            searchAndViewItems.Show();
        }

        private void viewBetweenTwoDatesReportsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            viewBetweenTwoDatesReportForm reportForm = new viewBetweenTwoDatesReportForm();
            reportForm.Show();
        }
    }
}
