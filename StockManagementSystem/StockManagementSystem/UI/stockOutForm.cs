﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using StockManagementSystem.Gateway;
using StockManagementSystem.Manager;
using StockManagementSystem.Models;
using Tulpep.NotificationWindow;

namespace StockManagementSystem.UI
{
    public partial class stockOutForm : Form
    {
        public stockOutForm()
        {
            InitializeComponent();
        }
        Item item = new Item();
        ItemManager itemManager = new ItemManager();
        StockInManager stockInManager = new StockInManager();
        StockOutManager stockOutManager = new StockOutManager();
        CategoryManager categoryManager = new CategoryManager();
        CompanyManager companyManager = new CompanyManager();
        UserManager userManager = new UserManager();
        List<Item> items = new List<Item>();

        private void stockOutForm_Load(object sender, EventArgs e)
        {
            try
            {
                LoadCompaniesComboBox();
                reorderLevelMessageLabel.Text = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void LoadCompaniesComboBox()
        {
            try
            {
                List<Company> companies = companyManager.GetCompaies();
                Company company = new Company();
                company.Id = 0;
                company.CompanyName = "--Select--";
                companies.Insert(0, company);
                companiesComboBox.DataSource = null;
                companiesComboBox.DataSource = companies;
                companiesComboBox.DisplayMember = "CompanyName";
                companiesComboBox.ValueMember = "Id";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void companiesComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (companiesComboBox.SelectedIndex > 0)
                {
                    Company company = new Company();
                    company.Id = Convert.ToInt32(companiesComboBox.SelectedValue);
                    List<Category> categories = categoryManager.GetCategoriesByCompany(company);
                    if (categories.Count > 0)
                    {
                        Category category = new Category();
                        category.Id = 0;
                        category.CategoryName = "--Select--";
                        categories.Insert(0, category);
                        categoriesComboBox.DataSource = null;
                        categoriesComboBox.DataSource = categories;
                        categoriesComboBox.DisplayMember = "CategoryName";
                        categoriesComboBox.ValueMember = "Id";
                        reorderLevelMessageLabel.Text = "";
                    }
                    else
                    {
                        List<string> data = new List<string>();
                        data.Add("No Data Found!");
                        categoriesComboBox.DataSource = data;
                        reorderLevelMessageLabel.Text = "";
                    }
                }
                else
                {
                    categoriesComboBox.DataSource = null;
                    itemsComboBox.DataSource = null;
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void categoriesComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (categoriesComboBox.SelectedIndex > 0)
                {
                    int categoryId = Convert.ToInt32(categoriesComboBox.SelectedValue);
                    int companyId = Convert.ToInt32(companiesComboBox.SelectedValue);
                    List<Item> items = itemManager.GetItemsByCaGetItemsByCategoryAndCompanytegory(categoryId, companyId);
                    if (items.Count > 0)
                    {
                        Item item = new Item();
                        item.Id = 0;
                        item.ItemName = "--Select--";
                        items.Insert(0, item);
                        itemsComboBox.DataSource = null;
                        itemsComboBox.DataSource = items;
                        itemsComboBox.DisplayMember = "ItemName";
                        itemsComboBox.ValueMember = "Id";
                        reorderLevelMessageLabel.Text = "";
                    }
                    else
                    {
                        List<string> data = new List<string>();
                        data.Add("No Data Found!");
                        itemsComboBox.DataSource = data;
                        reorderLevelMessageLabel.Text = "";
                    }
                }
                else
                {
                    itemsComboBox.DataSource = null;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void itemsComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (itemsComboBox.SelectedIndex > 0)
                {
                    item.Id = Convert.ToInt32(itemsComboBox.SelectedValue);
                    reorderLeveTextBox.Text = itemManager.GetItemReorderLevel(item).ToString();
                    int availableQuantity = itemManager.GetAvailableQuantity(item.Id);
                    availableQuantityTextBox.Text = availableQuantity.ToString();
                    reorderLevelMessageLabel.Text = "";
                }
                else
                {
                    reorderLeveTextBox.Clear();
                    availableQuantityTextBox.Clear();
                    reorderLevelMessageLabel.Text = "";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (AddButton.Text == "&Add")
                {
                    if (IsFormValid())
                    {
                        Item item = new Item();
                        int reOrderLevel = Convert.ToInt32(reorderLeveTextBox.Text);
                        int availableQuantity = Convert.ToInt32(availableQuantityTextBox.Text);
                        if (availableQuantity <= 0)
                        {
                            MessageBox.Show("Insufficient Item!", "Insufficient", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            stockOutQuantityTextBox.Focus();
                            return;
                        }
                        item.StockOutQuantity = Convert.ToInt32(stockOutQuantityTextBox.Text);
                        item.Id = Convert.ToInt32(itemsComboBox.SelectedValue);
                        item.CategoryName = categoriesComboBox.Text;
                        item.CompanyName = companiesComboBox.Text;
                        item.ItemName = itemsComboBox.Text;
                        item.ReorderLevel = Convert.ToInt32(reorderLeveTextBox.Text);
                        item.AvailableQuantity = Convert.ToInt32(availableQuantityTextBox.Text);
                        if (item.StockOutQuantity <= 0)
                        {
                            MessageBox.Show("Quantity '0' is not accept!", "NULL", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            stockOutQuantityTextBox.Focus();
                            return;
                        }
                        if (item.StockOutQuantity > availableQuantity)
                        {
                            MessageBox.Show("Insufficient Item!", "Insufficient", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            stockOutQuantityTextBox.Focus();
                            return;
                        }
                        if (item.StockOutQuantity >= (availableQuantity - reOrderLevel))
                        {
                            reorderLevelMessageLabel.Text = "Please Restock This Item!";
                            //GetNotification();
                        }
                        if (items.Count > 0)
                        {
                            var dataItems = items.Where(c => c.Id == item.Id).ToList();
                            if (dataItems.Count > 0)
                            {
                                int checkQuantity = 0;
                                foreach (var data in dataItems)
                                {
                                    checkQuantity = data.StockOutQuantity;
                                    if ((item.StockOutQuantity+checkQuantity) > availableQuantity)
                                    {
                                        MessageBox.Show("Insufficient Item!", "Insufficient", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                        stockOutQuantityTextBox.Focus();
                                        return;
                                    }
                                    if ((item.StockOutQuantity+checkQuantity) >= (availableQuantity - reOrderLevel))
                                    {
                                        reorderLevelMessageLabel.Text = "Please Restock This Item!";
                                        //GetNotification();
                                    }
                                    data.StockOutQuantity += item.StockOutQuantity;
                                    BindItemsGridView(items);
                                    return;
                                }
                            }
                        }
                        items.Add(item);
                        BindItemsGridView(items);
                    }
                }
                if (AddButton.Text == "Update")
                {
                    item.Id = Convert.ToInt32(idLabel.Text);
                    item.StockOutQuantity = Convert.ToInt32(stockOutQuantityTextBox.Text);
                    item.AvailableQuantity = Convert.ToInt32(availableQuantityTextBox.Text);
                    item.ReorderLevel = Convert.ToInt32(reorderLeveTextBox.Text);
                    var dataItem = items.Where(c => c.Id == item.Id).ToList();
                    if (dataItem.Count > 0)
                    {
                        foreach (var data in dataItem)
                        {
                            if (item.StockOutQuantity > data.AvailableQuantity)
                            {
                                MessageBox.Show("Insufficient Item!", "Insufficient", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                stockOutQuantityTextBox.Focus();
                                return;
                            }
                            if (item.StockOutQuantity >= (data.AvailableQuantity - data.ReorderLevel))
                            {
                                reorderLevelMessageLabel.Text = "Please Restock This Item!";
                            }
                            data.StockOutQuantity = item.StockOutQuantity;
                        }
                    }
                    BindItemsGridView(items);
                    AddButton.Text = "&Add";
                    AddButton.BackColor = Color.Indigo;
                    companiesComboBox.Enabled = true;
                    categoriesComboBox.Enabled = true;
                    itemsComboBox.Enabled = true;
                    CancelButton.Visible = false;
                    idLabel.Text = "";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void BindItemsGridView(List<Item> items)
        {
            try
            {
                int serial = 0;
                itemsListGirdView.Rows.Clear();
                foreach (var item in items)
                {
                    serial++;
                    itemsListGirdView.Rows.Add(serial, item.ItemName, item.CompanyName, item.StockOutQuantity, item.Id, item.CategoryName, item.ReorderLevel);
                }
                itemsListGirdView.Sort(itemsListGirdView.Columns[0], ListSortDirection.Descending);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private bool IsFormValid()
        {
            if (companiesComboBox.SelectedIndex <= 0)
            {
                MessageBox.Show("Please select company name!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                companiesComboBox.Focus();
                return false;
            }
            if (categoriesComboBox.SelectedIndex <= 0)
            {
                MessageBox.Show("Please select category name!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                categoriesComboBox.Focus();
                return false;
            }
            if (itemsComboBox.SelectedIndex <= 0)
            {
                MessageBox.Show("Please select item name!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                itemsComboBox.Focus();
                return false;
            }
            if (stockOutQuantityTextBox.Text.Trim() == string.Empty)
            {
                MessageBox.Show("Please enter stock out quantity!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                stockOutQuantityTextBox.Clear();
                stockOutQuantityTextBox.Focus();
                return false;
            }
            return true;
        }

        private void stockOutQuantityTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (!char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                e.Handled = true;
            }
        }

        private void SellButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (IsFormValid())
                {
                    bool isSave = false;
                    if (items.Count <= 0)
                    {
                        MessageBox.Show("Please add your items!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    string outType = "Sell";
                    int userId = userManager.GetUserId(LoginForm.UserName, LoginForm.Password);
                    DateTime date = DateTime.Now;
                    foreach (var data in items)
                    {
                        int row = stockOutManager.SaveItemQuantityInStockOut(data.Id, outType, data.StockOutQuantity, userId, date);
                        if (row > 0)
                        {
                            isSave = true;
                        }
                    }
                    if (isSave)
                    {
                        MessageBox.Show("Stok Out Successful.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Reset();
                    }
                    else
                    {
                        MessageBox.Show("Stock Out Failed!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Reset()
        {
            itemsListGirdView.Rows.Clear();
            items.Clear();
            companiesComboBox.SelectedIndex = 0;
            stockOutQuantityTextBox.Clear();
        }

        private void LostButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (IsFormValid())
                {
                    bool isSave = false;
                    if (items.Count <= 0)
                    {
                        MessageBox.Show("Please add your items!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    string outType = "Lost";
                    int userId = userManager.GetUserId(LoginForm.UserName, LoginForm.Password);
                    DateTime date = DateTime.Now;
                    foreach (var data in items)
                    {
                        int row = stockOutManager.SaveItemQuantityInStockOut(data.Id, outType, data.StockOutQuantity, userId, date);
                        if (row > 0)
                        {
                            isSave = true;
                        }
                    }
                    if (isSave)
                    {
                        MessageBox.Show("Stok Out Successful.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Reset();
                    }
                    else
                    {
                        MessageBox.Show("Stock Out Failed!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void DamageButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (IsFormValid())
                {
                    bool isSave = false;
                    if (items.Count <= 0)
                    {
                        MessageBox.Show("Please add your items!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    string outType = "Damage";
                    int userId = userManager.GetUserId(LoginForm.UserName, LoginForm.Password);
                    DateTime date = DateTime.Now;
                    foreach (var data in items)
                    {
                        int row = stockOutManager.SaveItemQuantityInStockOut(data.Id, outType, data.StockOutQuantity, userId, date);
                        if (row > 0)
                        {
                            isSave = true;
                        }
                    }
                    if (isSave)
                    {
                        MessageBox.Show("Stok Out Successful.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Reset();
                    }
                    else
                    {
                        MessageBox.Show("Stock Out Failed!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void GetNotification()
        {
            PopupNotifier notifier = new PopupNotifier();
            //notifier.Image = Properties.Resources.notification01;
            notifier.Image = Properties.Resources.Hopstarter_Malware_Notification.ToBitmap();
            notifier.TitleText = "Alert";
            notifier.ContentColor = Color.Aqua;
            notifier.ContentText = "Please restock this item!";
            notifier.Popup();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            AddButton.Text = "&Add";
            AddButton.BackColor = Color.Indigo;
            companiesComboBox.Enabled = true;
            categoriesComboBox.Enabled = true;
            itemsComboBox.Enabled = true;
            companiesComboBox.SelectedIndex = 0;
            CancelButton.Visible = false;
            idLabel.Text = "";
        }

        private void itemsListGirdView_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                DataGridViewRow dr = itemsListGirdView.SelectedRows[0];
                companiesComboBox.Text = dr.Cells[2].Value.ToString();
                stockOutQuantityTextBox.Text = dr.Cells[3].Value.ToString();
                categoriesComboBox.Text = dr.Cells[5].Value.ToString();
                itemsComboBox.Text = dr.Cells[1].Value.ToString();
                reorderLeveTextBox.Text = dr.Cells[6].Value.ToString();
                idLabel.Text = dr.Cells[4].Value.ToString();
                availableQuantityTextBox.Text = itemManager.GetAvailableQuantity(Convert.ToInt32(dr.Cells[4].Value)).ToString();

                AddButton.Text = "Update";
                AddButton.BackColor = Color.Magenta;
                CancelButton.Visible = true;
                companiesComboBox.Enabled = false;
                categoriesComboBox.Enabled = false;
                itemsComboBox.Enabled = false;



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
