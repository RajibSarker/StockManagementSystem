﻿using StockManagementSystem.Gateway;
using StockManagementSystem.Manager;
using StockManagementSystem.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StockManagementSystem.UI
{
    public partial class itemSetupForm : Form
    {
        public itemSetupForm()
        {
            InitializeComponent();
        }
        Item item = new Item();
        ItemManager itemManager = new ItemManager();
        CategoryManager categoryManager = new CategoryManager();
        CompanyManager companyManager = new CompanyManager();
        UserManager userManager = new UserManager();
        
        private void itemSetupForm_Load(object sender, EventArgs e)
        {
            try
            {
                LoadCategoriesComboBox();
                LoadCompaniesComboBox();
                List<Item> items = itemManager.GetItems();
                BindItemsGridView(items);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void BindItemsGridView(List<Item> items)
        {
            try
            {
                int serial = 0;
                itemsGridView.Rows.Clear();
                foreach(var item in items)
                {
                    serial++;
                    itemsGridView.Rows.Add(serial, item.ItemName, item.Id, item.CompanyName, item.CategoryName, item.ReorderLevel);
                }
                itemsGridView.Sort(itemsGridView.Columns[0], ListSortDirection.Descending);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void LoadCompaniesComboBox()
        {
            try
            {
                List<Company> companies = companyManager.GetCompaies();
                Company company = new Company();
                company.Id = 0;
                company.CompanyName = "--Select--";
                companies.Insert(0, company);
                companiesComboBox.DataSource = null;
                companiesComboBox.DataSource = companies;
                companiesComboBox.DisplayMember = "CompanyName";
                companiesComboBox.ValueMember = "Id";
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void LoadCategoriesComboBox()
        {
            try
            {
                List<Category> categories = categoryManager.GetCategories();
                Category category = new Category();
                category.Id = 0;
                category.CategoryName = "---Select---";
                categories.Insert(0, category);
                categoriesComboBox.DataSource = null;
                categoriesComboBox.DataSource = categories;
                categoriesComboBox.DisplayMember = "CategoryName";
                categoriesComboBox.ValueMember = "Id";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (SaveButton.Text == "&Save")
                {
                    if (IsFormValid())
                    {
                        item.CategoryId = Convert.ToInt32(categoriesComboBox.SelectedValue);
                        item.CompanyId = Convert.ToInt32(companiesComboBox.SelectedValue);
                        item.ItemName = itemNameTextBox.Text;
                        item.ReorderLevel = Convert.ToInt32(reorderLeveTextBox.Text);
                        item.UserId = userManager.GetUserId(LoginForm.UserName, LoginForm.Password);
                        item.CreatedDate = DateTime.Now;
                        item.ActionType = "Insert";
                        if (itemManager.IsExistItem(item))
                        {
                            MessageBox.Show("Item alredy exist!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }
                        string message = itemManager.SaveItem(item);
                        if (message == "Item Save Successful.")
                        {
                            MessageBox.Show(message, "Successful", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            categoriesComboBox.SelectedIndex = 0;
                            companiesComboBox.SelectedIndex = 0;
                            List<Item> items = itemManager.GetItems();
                            BindItemsGridView(items);
                            itemNameTextBox.Clear();
                            reorderLeveTextBox.Text = "0";
                        }
                        else
                        {
                            MessageBox.Show(message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
                if (SaveButton.Text == "Update")
                {
                    if (IsFormValid())
                    {
                        item.Id = Convert.ToInt32(idLabel.Text);
                        item.ItemName = itemNameTextBox.Text;
                        item.ReorderLevel = Convert.ToInt32(reorderLeveTextBox.Text);
                        item.UserId = userManager.GetUserId(LoginForm.UserName, LoginForm.Password);
                        item.CreatedDate = DateTime.Now;
                        item.ActionType = "Update";
                        if (itemManager.IsExistItem(item))
                        {
                            MessageBox.Show("Item alredy exist!", "Exist", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }
                        int isExecuted = itemManager.UpdateItem(item);
                        if (isExecuted > 0)
                        {
                            MessageBox.Show("Update successful.", "Updated", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            List<Item> items = itemManager.GetItems();
                            BindItemsGridView(items);
                            Reset();
                        }
                        else
                        {
                            MessageBox.Show("Update failed!", "Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }
                    }
                }
            }
            catch(Exception exception)
            {
                MessageBox.Show(exception.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private bool IsFormValid()
        {
            if (categoriesComboBox.SelectedIndex == 0)
            {
                MessageBox.Show("Please Select Category!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                categoriesComboBox.Focus();
                return false;
            }
            if (companiesComboBox.SelectedIndex == 0)
            {
                MessageBox.Show("Please Select Company!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                companiesComboBox.Focus();
                return false;
            }
            if (itemNameTextBox.Text.Trim() == string.Empty)
            {
                MessageBox.Show("Please Enter Item Name!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                itemNameTextBox.Focus();
                itemNameTextBox.Clear();
                return false;
            }
            return true;
        }

        private void reorderLeveTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (!char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                e.Handled = true;
            }
        }

        private void itemsGridView_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                DataGridViewRow dr = itemsGridView.SelectedRows[0];
                itemNameTextBox.Text = dr.Cells[1].Value.ToString();
                idLabel.Text = dr.Cells[2].Value.ToString();
                companiesComboBox.Text = dr.Cells[3].Value.ToString();
                categoriesComboBox.Text = dr.Cells[4].Value.ToString();
                reorderLeveTextBox.Text = dr.Cells[5].Value.ToString();
                SaveButton.Text = "Update";
                CancelButton.Visible = true;
                SaveButton.BackColor = Color.Magenta;
                companiesComboBox.Enabled = false;
                categoriesComboBox.Enabled = false;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            Reset();
        }

        private void Reset()
        {
            SaveButton.Text = "&Save";
            SaveButton.BackColor = Color.Indigo;
            companiesComboBox.Enabled = true;
            categoriesComboBox.Enabled = true;
            companiesComboBox.SelectedIndex = 0;
            categoriesComboBox.SelectedIndex = 0;
            reorderLeveTextBox.Text = "0";
            itemNameTextBox.Clear();
            idLabel.Text = "";
            CancelButton.Visible = false;
        }
    }
}
    