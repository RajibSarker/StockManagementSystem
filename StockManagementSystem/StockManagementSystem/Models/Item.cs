﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockManagementSystem.Models
{
    class Item
    {
        public int Id { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public int CompanyId { get; set; }
        public string CompanyName { get; set; }
        public string ItemName { get; set; }
        public int ReorderLevel { get; set; }
        public int StockInQuantity { get; set; }
        public int StockOutQuantity { get; set; }
        public string StockOutType { get; set; }    
        public int AvailableQuantity { get; set; }
        public int StockInId { get; set; }
        public int UserId { get; set; }
        public string ActionType { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string Date { get; set; }

    }
}
