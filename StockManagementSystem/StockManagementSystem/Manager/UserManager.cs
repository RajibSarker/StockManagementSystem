﻿using StockManagementSystem.Gateway;
using StockManagementSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockManagementSystem.Manager
{
    public class UserManager
    {
        UserGateway userGateway = new UserGateway();
        public int IsExistUser(User user)
        {
            int count = userGateway.IsExistUser(user);
            if (count > 0)
            {
                return count;
            }
            return count;
        }
        public int GetUserId(string userName,string password)
        {
            int userId = userGateway.GetUserId(userName, password);
            if (userId > 0)
            {
                return userId;
            }
            return userId;
        }
    }
}
