﻿using StockManagementSystem.Gateway;
using StockManagementSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockManagementSystem.Manager
{
    class CategoryManager
    {
        CategoryGateway categoryGateway = new CategoryGateway();
        public string SaveCategory(Category category)
        {
            int row = categoryGateway.SaveCategory(category);
            if (row > 0)
            {
                return "Category Saved Successful.";
            }
            return "Save Failed!";
        }
        public List<Category> GetCategories()
        {
            return categoryGateway.GetCategories();
        }
        public string UpdateCategory(Category category)
        {
            int row = categoryGateway.UpdateCategory(category);
            if (row > 0)
            {
                return "Category Update Successful.";
            }
            return "Update Failed!";
        }
        public bool IsExistCategory(Category category)
        {
            if (categoryGateway.IsExistCategory(category))
            {
                return true;
            }
            return false;
        }
        public List<Category> GetCategoriesByCompany(Company company)
        {
            return categoryGateway.GetCategoriesByCompany(company);
        }
    }
    
}
