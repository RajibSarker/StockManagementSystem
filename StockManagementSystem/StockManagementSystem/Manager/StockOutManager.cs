﻿using StockManagementSystem.Gateway;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StockManagementSystem.Models;

namespace StockManagementSystem.Manager
{
    class StockOutManager
    {
        StockOutGateway stockOutGateway = new StockOutGateway();
        public int SaveItemQuantityInStockOut(int itemId, string outType, int quantity, int userId, DateTime date)
        {
            return stockOutGateway.SaveItemQuantityInStockOut(itemId,outType,quantity,userId,date);
        }
        public int GetItemStockOutQuantity(int id)
        {
            return stockOutGateway.GetItemStockOutQuantity(id);
        }
        public List<Item> GetItemsByDateAndOutTpe(Item item)
        {
            return stockOutGateway.GetItemsByDateAndOutTpe(item);
        }
    }
}
