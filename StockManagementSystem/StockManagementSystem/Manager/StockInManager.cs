﻿using StockManagementSystem.Gateway;
using System;
using System.Collections.Generic;
using StockManagementSystem.Models;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockManagementSystem.Manager
{
    class StockInManager
    {
        StockInGateway stockInGateway = new StockInGateway();
        public int SaveItemQuantityInStockIn(Item item)
        {
            return stockInGateway.SaveItemQuantityInStockIn(item);
        }
        public int UpdateItemQuantityInStockIn(Item item)
        {
            int row = stockInGateway.UpdateItemQuantityInStockIn(item);
            if (row > 0)
            {
                return row;
            }
            return row;
        }
        public List<Item> GetItemsInStockIn()
        {
            return stockInGateway.GetItemsInStockIn();
        }
        public int GetItemAvailableQuantity(int id)
        {
            return stockInGateway.GetItemAvailableQuantity(id);
        }
    }
}
