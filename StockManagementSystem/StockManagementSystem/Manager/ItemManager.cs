﻿using StockManagementSystem.Gateway;
using StockManagementSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockManagementSystem.Manager
{
    class ItemManager
    {
        ItemGateway itemGateway = new ItemGateway();
        StockInGateway stockInGateway = new StockInGateway();
        StockOutGateway stockOutGateway = new StockOutGateway();

        public string SaveItem(Item item)
        {
            int row = itemGateway.SaveItem(item);
            if (row > 0)
            {
                return "Item Save Successful.";
            }
            else
            {
                return "Save Failed!";
            }
        }
        public int UpdateItem(Item item)
        {
            return itemGateway.UpdateItem(item);
        }
        public List<Item> GetItems()
        {
            return itemGateway.GetItems();
        }
        public bool IsExistItem(Item item)
        {
            if (itemGateway.IsExistItem(item))
            {
                return true;
            }
            return false;
        }
        public List<Item> GetItemsByCaGetItemsByCategoryAndCompanytegory(int categoryId,int companyId)
        {
            return itemGateway.GetItemsByCategoryAndCompany(categoryId, companyId);
        }
        public int GetItemReorderLevel(Item item)
        {
            return itemGateway.GetItemReorderLevel(item);
        }
        public int GetAvailableQuantity(int id)
        {
            return stockInGateway.GetItemAvailableQuantity(id) - stockOutGateway.GetItemStockOutQuantity(id);
        }
    }
}
