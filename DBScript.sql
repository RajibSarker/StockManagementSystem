USE [StockManagementSystem]
GO
/****** Object:  Table [dbo].[Categories]    Script Date: 07/07/19 9:37:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Categories](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NULL,
	[UserId] [int] NULL,
	[CreatedDate] [date] NULL,
 CONSTRAINT [PK_Categories] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Items]    Script Date: 07/07/19 9:37:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Items](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NULL,
	[ReOrderLevel] [int] NULL,
	[CategoryId] [int] NULL,
	[CompanyId] [int] NOT NULL,
	[UserId] [int] NULL,
	[CreatedDate] [date] NULL,
 CONSTRAINT [PK_Items] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[ItemView]    Script Date: 07/07/19 9:37:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create View [dbo].[ItemView]
AS
Select i.Name as ItemName,i.ReOrderLevel, c.Name as CategoryName From Items as i INNER JOIN Categories as c ON i.CategoryId=c.Id
GO
/****** Object:  Table [dbo].[Companys]    Script Date: 07/07/19 9:37:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Companys](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NULL,
	[UserId] [int] NULL,
	[CreatedDate] [date] NULL,
 CONSTRAINT [PK_Companys] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StockIn]    Script Date: 07/07/19 9:37:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StockIn](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ItemId] [int] NULL,
	[Quantity] [int] NULL,
	[UserId] [int] NULL,
	[CreatedDate] [date] NULL,
 CONSTRAINT [PK_StockIn] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StockOut]    Script Date: 07/07/19 9:37:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StockOut](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ItemId] [int] NULL,
	[OutType] [nvarchar](100) NULL,
	[Quantity] [int] NULL,
	[UserId] [int] NULL,
	[CreatedDate] [date] NULL,
 CONSTRAINT [PK_StockOut] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 07/07/19 9:37:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](100) NULL,
	[Password] [nvarchar](100) NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Categories] ON 

INSERT [dbo].[Categories] ([Id], [Name], [UserId], [CreatedDate]) VALUES (1, N'Electronics', 2, CAST(N'2019-07-05' AS Date))
INSERT [dbo].[Categories] ([Id], [Name], [UserId], [CreatedDate]) VALUES (2, N'Statonaries', NULL, CAST(N'2019-06-15' AS Date))
INSERT [dbo].[Categories] ([Id], [Name], [UserId], [CreatedDate]) VALUES (3, N'Cosmetics', NULL, CAST(N'2019-06-15' AS Date))
INSERT [dbo].[Categories] ([Id], [Name], [UserId], [CreatedDate]) VALUES (4, N'Kitchen Items', 2, CAST(N'2019-07-05' AS Date))
INSERT [dbo].[Categories] ([Id], [Name], [UserId], [CreatedDate]) VALUES (5, N'New Items', NULL, CAST(N'2019-06-15' AS Date))
INSERT [dbo].[Categories] ([Id], [Name], [UserId], [CreatedDate]) VALUES (6, N'Computers Item', NULL, CAST(N'2019-06-15' AS Date))
INSERT [dbo].[Categories] ([Id], [Name], [UserId], [CreatedDate]) VALUES (7, N'Mobiles', NULL, CAST(N'2019-06-15' AS Date))
INSERT [dbo].[Categories] ([Id], [Name], [UserId], [CreatedDate]) VALUES (8, N'Laptop', NULL, CAST(N'2019-06-15' AS Date))
INSERT [dbo].[Categories] ([Id], [Name], [UserId], [CreatedDate]) VALUES (10, N'Watch', NULL, CAST(N'2019-06-16' AS Date))
INSERT [dbo].[Categories] ([Id], [Name], [UserId], [CreatedDate]) VALUES (11, N'ACI Foods Limited', NULL, CAST(N'2019-06-17' AS Date))
INSERT [dbo].[Categories] ([Id], [Name], [UserId], [CreatedDate]) VALUES (12, N'ACI Electronics ', NULL, CAST(N'2019-06-17' AS Date))
INSERT [dbo].[Categories] ([Id], [Name], [UserId], [CreatedDate]) VALUES (14, N'Foods', NULL, CAST(N'2019-06-22' AS Date))
INSERT [dbo].[Categories] ([Id], [Name], [UserId], [CreatedDate]) VALUES (15, N'Icecreams', NULL, CAST(N'2019-06-22' AS Date))
INSERT [dbo].[Categories] ([Id], [Name], [UserId], [CreatedDate]) VALUES (16, N'Waters', 1, CAST(N'2019-06-22' AS Date))
INSERT [dbo].[Categories] ([Id], [Name], [UserId], [CreatedDate]) VALUES (1014, N'Camera', 2, CAST(N'2019-06-26' AS Date))
INSERT [dbo].[Categories] ([Id], [Name], [UserId], [CreatedDate]) VALUES (1015, N'Mobile', 2, CAST(N'2019-07-03' AS Date))
INSERT [dbo].[Categories] ([Id], [Name], [UserId], [CreatedDate]) VALUES (1016, N'Drinks', 2, CAST(N'2019-07-05' AS Date))
INSERT [dbo].[Categories] ([Id], [Name], [UserId], [CreatedDate]) VALUES (1017, N'New Categorys', 2, CAST(N'2019-07-05' AS Date))
INSERT [dbo].[Categories] ([Id], [Name], [UserId], [CreatedDate]) VALUES (1018, N'UPs', 2, CAST(N'2019-07-07' AS Date))
SET IDENTITY_INSERT [dbo].[Categories] OFF
SET IDENTITY_INSERT [dbo].[Companys] ON 

INSERT [dbo].[Companys] ([Id], [Name], [UserId], [CreatedDate]) VALUES (1, N'IBCS Primax Software Bangladesh Ltd.', 2, CAST(N'2019-07-05' AS Date))
INSERT [dbo].[Companys] ([Id], [Name], [UserId], [CreatedDate]) VALUES (2, N'Silika Bangladesh Ltd', NULL, CAST(N'2019-06-17' AS Date))
INSERT [dbo].[Companys] ([Id], [Name], [UserId], [CreatedDate]) VALUES (3, N'Pran-RFL Groups', 2, CAST(N'2019-07-05' AS Date))
INSERT [dbo].[Companys] ([Id], [Name], [UserId], [CreatedDate]) VALUES (4, N'ACI Groups', 2, CAST(N'2019-07-06' AS Date))
INSERT [dbo].[Companys] ([Id], [Name], [UserId], [CreatedDate]) VALUES (5, N'Navana Group', NULL, CAST(N'2019-06-17' AS Date))
INSERT [dbo].[Companys] ([Id], [Name], [UserId], [CreatedDate]) VALUES (6, N'Beximco Group', NULL, CAST(N'2019-06-17' AS Date))
INSERT [dbo].[Companys] ([Id], [Name], [UserId], [CreatedDate]) VALUES (7, N'Square Group', NULL, CAST(N'2019-06-17' AS Date))
INSERT [dbo].[Companys] ([Id], [Name], [UserId], [CreatedDate]) VALUES (8, N'City Group', NULL, CAST(N'2019-06-17' AS Date))
INSERT [dbo].[Companys] ([Id], [Name], [UserId], [CreatedDate]) VALUES (9, N'Partex Group', NULL, CAST(N'2019-06-17' AS Date))
INSERT [dbo].[Companys] ([Id], [Name], [UserId], [CreatedDate]) VALUES (10, N'Ananda Group', NULL, CAST(N'2019-06-17' AS Date))
INSERT [dbo].[Companys] ([Id], [Name], [UserId], [CreatedDate]) VALUES (11, N'Walton LTD', NULL, CAST(N'2019-06-17' AS Date))
INSERT [dbo].[Companys] ([Id], [Name], [UserId], [CreatedDate]) VALUES (12, N'Our New Company LTD.', 1, CAST(N'2019-06-22' AS Date))
INSERT [dbo].[Companys] ([Id], [Name], [UserId], [CreatedDate]) VALUES (1012, N'Travle Win', 2, CAST(N'2019-06-23' AS Date))
INSERT [dbo].[Companys] ([Id], [Name], [UserId], [CreatedDate]) VALUES (1013, N'Apple Inc.', 2, CAST(N'2019-06-26' AS Date))
INSERT [dbo].[Companys] ([Id], [Name], [UserId], [CreatedDate]) VALUES (1014, N'Samsung Ltd.', 2, CAST(N'2019-06-26' AS Date))
INSERT [dbo].[Companys] ([Id], [Name], [UserId], [CreatedDate]) VALUES (1015, N'OPPO Company LTD', 2, CAST(N'2019-07-03' AS Date))
INSERT [dbo].[Companys] ([Id], [Name], [UserId], [CreatedDate]) VALUES (1016, N'Cocacola Group LTD.', 2, CAST(N'2019-07-05' AS Date))
INSERT [dbo].[Companys] ([Id], [Name], [UserId], [CreatedDate]) VALUES (1017, N'The Power Groups', 2, CAST(N'2019-07-05' AS Date))
INSERT [dbo].[Companys] ([Id], [Name], [UserId], [CreatedDate]) VALUES (1018, N'New Company', 2, CAST(N'2019-07-05' AS Date))
INSERT [dbo].[Companys] ([Id], [Name], [UserId], [CreatedDate]) VALUES (1019, N'New Companys', 2, CAST(N'2019-07-05' AS Date))
INSERT [dbo].[Companys] ([Id], [Name], [UserId], [CreatedDate]) VALUES (1020, N'Smart''s Technology LTD', 2, CAST(N'2019-07-07' AS Date))
SET IDENTITY_INSERT [dbo].[Companys] OFF
SET IDENTITY_INSERT [dbo].[Items] ON 

INSERT [dbo].[Items] ([Id], [Name], [ReOrderLevel], [CategoryId], [CompanyId], [UserId], [CreatedDate]) VALUES (1, N'Names', 5, 1, 1, 2, CAST(N'2019-06-17' AS Date))
INSERT [dbo].[Items] ([Id], [Name], [ReOrderLevel], [CategoryId], [CompanyId], [UserId], [CreatedDate]) VALUES (11, N'Vishon LED', 5, 1, 2, 2, CAST(N'2019-06-17' AS Date))
INSERT [dbo].[Items] ([Id], [Name], [ReOrderLevel], [CategoryId], [CompanyId], [UserId], [CreatedDate]) VALUES (12, N'Laptops', 6, 1, 2, 2, CAST(N'2019-06-17' AS Date))
INSERT [dbo].[Items] ([Id], [Name], [ReOrderLevel], [CategoryId], [CompanyId], [UserId], [CreatedDate]) VALUES (13, N'Titan', 10, 10, 11, 2, CAST(N'2019-06-17' AS Date))
INSERT [dbo].[Items] ([Id], [Name], [ReOrderLevel], [CategoryId], [CompanyId], [UserId], [CreatedDate]) VALUES (14, N'Rulex', 10, 10, 11, 1, CAST(N'2019-06-17' AS Date))
INSERT [dbo].[Items] ([Id], [Name], [ReOrderLevel], [CategoryId], [CompanyId], [UserId], [CreatedDate]) VALUES (15, N'Walton Prime', 5, 7, 11, 1, CAST(N'2019-06-17' AS Date))
INSERT [dbo].[Items] ([Id], [Name], [ReOrderLevel], [CategoryId], [CompanyId], [UserId], [CreatedDate]) VALUES (16, N'Mobile Charger', 15, 1, 4, 2, CAST(N'2019-06-17' AS Date))
INSERT [dbo].[Items] ([Id], [Name], [ReOrderLevel], [CategoryId], [CompanyId], [UserId], [CreatedDate]) VALUES (17, N'Acear', 5, 8, 10, 2, CAST(N'2019-06-17' AS Date))
INSERT [dbo].[Items] ([Id], [Name], [ReOrderLevel], [CategoryId], [CompanyId], [UserId], [CreatedDate]) VALUES (18, N'Mouse', 20, 6, 10, 1, CAST(N'2019-06-17' AS Date))
INSERT [dbo].[Items] ([Id], [Name], [ReOrderLevel], [CategoryId], [CompanyId], [UserId], [CreatedDate]) VALUES (19, N'Name', 5, 1, 1, 2, CAST(N'2019-06-19' AS Date))
INSERT [dbo].[Items] ([Id], [Name], [ReOrderLevel], [CategoryId], [CompanyId], [UserId], [CreatedDate]) VALUES (20, N'Name1', 5, 1, 1, 1, CAST(N'2019-06-19' AS Date))
INSERT [dbo].[Items] ([Id], [Name], [ReOrderLevel], [CategoryId], [CompanyId], [UserId], [CreatedDate]) VALUES (1019, N'Navana Icecream', 50, 15, 5, 1, CAST(N'2019-06-22' AS Date))
INSERT [dbo].[Items] ([Id], [Name], [ReOrderLevel], [CategoryId], [CompanyId], [UserId], [CreatedDate]) VALUES (1020, N'City Drinking Water', 20, 16, 8, 2, CAST(N'2019-06-22' AS Date))
INSERT [dbo].[Items] ([Id], [Name], [ReOrderLevel], [CategoryId], [CompanyId], [UserId], [CreatedDate]) VALUES (2019, N'Burger', 20, 14, 12, 2, CAST(N'2019-06-23' AS Date))
INSERT [dbo].[Items] ([Id], [Name], [ReOrderLevel], [CategoryId], [CompanyId], [UserId], [CreatedDate]) VALUES (2020, N'Iphone 6 Plus', 10, 7, 1013, 2, CAST(N'2019-06-26' AS Date))
INSERT [dbo].[Items] ([Id], [Name], [ReOrderLevel], [CategoryId], [CompanyId], [UserId], [CreatedDate]) VALUES (2021, N'Apple-01', 5, 8, 1013, 2, CAST(N'2019-06-26' AS Date))
INSERT [dbo].[Items] ([Id], [Name], [ReOrderLevel], [CategoryId], [CompanyId], [UserId], [CreatedDate]) VALUES (2022, N'Iphone 7', 10, 7, 1013, 2, CAST(N'2019-06-26' AS Date))
INSERT [dbo].[Items] ([Id], [Name], [ReOrderLevel], [CategoryId], [CompanyId], [UserId], [CreatedDate]) VALUES (2023, N'Iphone 8', 10, 7, 1013, 2, CAST(N'2019-06-26' AS Date))
INSERT [dbo].[Items] ([Id], [Name], [ReOrderLevel], [CategoryId], [CompanyId], [UserId], [CreatedDate]) VALUES (2024, N'Iphone 8 Plus', 10, 7, 1013, 2, CAST(N'2019-06-26' AS Date))
INSERT [dbo].[Items] ([Id], [Name], [ReOrderLevel], [CategoryId], [CompanyId], [UserId], [CreatedDate]) VALUES (2025, N'SM-C207', 5, 1014, 1014, 2, CAST(N'2019-06-26' AS Date))
INSERT [dbo].[Items] ([Id], [Name], [ReOrderLevel], [CategoryId], [CompanyId], [UserId], [CreatedDate]) VALUES (2026, N'OPPO A33f', 10, 1015, 1015, 2, CAST(N'2019-07-03' AS Date))
INSERT [dbo].[Items] ([Id], [Name], [ReOrderLevel], [CategoryId], [CompanyId], [UserId], [CreatedDate]) VALUES (2027, N'The Cocacola-1LT.', 50, 1016, 1016, 2, CAST(N'2019-07-05' AS Date))
INSERT [dbo].[Items] ([Id], [Name], [ReOrderLevel], [CategoryId], [CompanyId], [UserId], [CreatedDate]) VALUES (2028, N'The Power-500ml', 40, 1016, 1017, 2, CAST(N'2019-07-05' AS Date))
INSERT [dbo].[Items] ([Id], [Name], [ReOrderLevel], [CategoryId], [CompanyId], [UserId], [CreatedDate]) VALUES (2029, N'New Drinks', 45, 1016, 1016, 2, CAST(N'2019-07-05' AS Date))
INSERT [dbo].[Items] ([Id], [Name], [ReOrderLevel], [CategoryId], [CompanyId], [UserId], [CreatedDate]) VALUES (2030, N'Cocacola Foods', 40, 14, 1016, 2, CAST(N'2019-07-05' AS Date))
INSERT [dbo].[Items] ([Id], [Name], [ReOrderLevel], [CategoryId], [CompanyId], [UserId], [CreatedDate]) VALUES (2031, N'New Items', 25, 1017, 1019, 2, CAST(N'2019-07-05' AS Date))
INSERT [dbo].[Items] ([Id], [Name], [ReOrderLevel], [CategoryId], [CompanyId], [UserId], [CreatedDate]) VALUES (2032, N'New Item', 25, 1017, 1019, 2, CAST(N'2019-07-05' AS Date))
INSERT [dbo].[Items] ([Id], [Name], [ReOrderLevel], [CategoryId], [CompanyId], [UserId], [CreatedDate]) VALUES (2033, N'Apple 9x', 15, 7, 1013, 2, CAST(N'2019-07-06' AS Date))
INSERT [dbo].[Items] ([Id], [Name], [ReOrderLevel], [CategoryId], [CompanyId], [UserId], [CreatedDate]) VALUES (2034, N'UPS-100vs', 5, 1018, 1020, 2, CAST(N'2019-07-07' AS Date))
INSERT [dbo].[Items] ([Id], [Name], [ReOrderLevel], [CategoryId], [CompanyId], [UserId], [CreatedDate]) VALUES (2035, N'UPS-wa455', 10, 1018, 11, 2, CAST(N'2019-07-07' AS Date))
SET IDENTITY_INSERT [dbo].[Items] OFF
SET IDENTITY_INSERT [dbo].[StockIn] ON 

INSERT [dbo].[StockIn] ([Id], [ItemId], [Quantity], [UserId], [CreatedDate]) VALUES (2, 14, 250, 2, CAST(N'2019-06-24' AS Date))
INSERT [dbo].[StockIn] ([Id], [ItemId], [Quantity], [UserId], [CreatedDate]) VALUES (3, 14, 30, 1, CAST(N'2019-06-20' AS Date))
INSERT [dbo].[StockIn] ([Id], [ItemId], [Quantity], [UserId], [CreatedDate]) VALUES (4, 14, 20, 1, CAST(N'2019-06-20' AS Date))
INSERT [dbo].[StockIn] ([Id], [ItemId], [Quantity], [UserId], [CreatedDate]) VALUES (5, 18, 30, 1, CAST(N'2019-06-20' AS Date))
INSERT [dbo].[StockIn] ([Id], [ItemId], [Quantity], [UserId], [CreatedDate]) VALUES (6, 18, 45, 2, CAST(N'2019-06-23' AS Date))
INSERT [dbo].[StockIn] ([Id], [ItemId], [Quantity], [UserId], [CreatedDate]) VALUES (7, 17, 100, 1, CAST(N'2019-06-20' AS Date))
INSERT [dbo].[StockIn] ([Id], [ItemId], [Quantity], [UserId], [CreatedDate]) VALUES (1002, 12, 45, 1, CAST(N'2019-06-22' AS Date))
INSERT [dbo].[StockIn] ([Id], [ItemId], [Quantity], [UserId], [CreatedDate]) VALUES (1003, 12, 10, 1, CAST(N'2019-06-22' AS Date))
INSERT [dbo].[StockIn] ([Id], [ItemId], [Quantity], [UserId], [CreatedDate]) VALUES (1004, 14, 50, 1, CAST(N'2019-06-22' AS Date))
INSERT [dbo].[StockIn] ([Id], [ItemId], [Quantity], [UserId], [CreatedDate]) VALUES (1005, 11, 50, 1, CAST(N'2019-06-22' AS Date))
INSERT [dbo].[StockIn] ([Id], [ItemId], [Quantity], [UserId], [CreatedDate]) VALUES (1006, 17, 20, 1, CAST(N'2019-06-22' AS Date))
INSERT [dbo].[StockIn] ([Id], [ItemId], [Quantity], [UserId], [CreatedDate]) VALUES (1007, 15, 25, 1, CAST(N'2019-06-22' AS Date))
INSERT [dbo].[StockIn] ([Id], [ItemId], [Quantity], [UserId], [CreatedDate]) VALUES (1008, 16, 80, 1, CAST(N'2019-06-22' AS Date))
INSERT [dbo].[StockIn] ([Id], [ItemId], [Quantity], [UserId], [CreatedDate]) VALUES (1009, 11, 20, 1, CAST(N'2019-06-22' AS Date))
INSERT [dbo].[StockIn] ([Id], [ItemId], [Quantity], [UserId], [CreatedDate]) VALUES (1010, 1019, 520, 1, CAST(N'2019-06-22' AS Date))
INSERT [dbo].[StockIn] ([Id], [ItemId], [Quantity], [UserId], [CreatedDate]) VALUES (1011, 1019, 30, 1, CAST(N'2019-06-22' AS Date))
INSERT [dbo].[StockIn] ([Id], [ItemId], [Quantity], [UserId], [CreatedDate]) VALUES (1012, 1020, 250, 2, CAST(N'2019-06-22' AS Date))
INSERT [dbo].[StockIn] ([Id], [ItemId], [Quantity], [UserId], [CreatedDate]) VALUES (2002, 2019, 55, 2, CAST(N'2019-06-23' AS Date))
INSERT [dbo].[StockIn] ([Id], [ItemId], [Quantity], [UserId], [CreatedDate]) VALUES (2003, 14, 50, 2, CAST(N'2019-06-24' AS Date))
INSERT [dbo].[StockIn] ([Id], [ItemId], [Quantity], [UserId], [CreatedDate]) VALUES (2004, 14, 100, 2, CAST(N'2019-06-24' AS Date))
INSERT [dbo].[StockIn] ([Id], [ItemId], [Quantity], [UserId], [CreatedDate]) VALUES (2005, 14, 40, 2, CAST(N'2019-06-24' AS Date))
INSERT [dbo].[StockIn] ([Id], [ItemId], [Quantity], [UserId], [CreatedDate]) VALUES (2006, 13, 100, 2, CAST(N'2019-06-26' AS Date))
INSERT [dbo].[StockIn] ([Id], [ItemId], [Quantity], [UserId], [CreatedDate]) VALUES (2007, 2024, 20, 2, CAST(N'2019-06-26' AS Date))
INSERT [dbo].[StockIn] ([Id], [ItemId], [Quantity], [UserId], [CreatedDate]) VALUES (2008, 2021, 15, 2, CAST(N'2019-06-26' AS Date))
INSERT [dbo].[StockIn] ([Id], [ItemId], [Quantity], [UserId], [CreatedDate]) VALUES (2009, 2025, 20, 2, CAST(N'2019-06-26' AS Date))
INSERT [dbo].[StockIn] ([Id], [ItemId], [Quantity], [UserId], [CreatedDate]) VALUES (2010, 2026, 50, 2, CAST(N'2019-07-03' AS Date))
INSERT [dbo].[StockIn] ([Id], [ItemId], [Quantity], [UserId], [CreatedDate]) VALUES (2011, 2026, 20, 2, CAST(N'2019-07-03' AS Date))
INSERT [dbo].[StockIn] ([Id], [ItemId], [Quantity], [UserId], [CreatedDate]) VALUES (2012, 2021, 3, 2, CAST(N'2019-07-04' AS Date))
INSERT [dbo].[StockIn] ([Id], [ItemId], [Quantity], [UserId], [CreatedDate]) VALUES (2013, 2021, 7, 2, CAST(N'2019-07-04' AS Date))
INSERT [dbo].[StockIn] ([Id], [ItemId], [Quantity], [UserId], [CreatedDate]) VALUES (2014, 2021, 20, 2, CAST(N'2019-07-04' AS Date))
INSERT [dbo].[StockIn] ([Id], [ItemId], [Quantity], [UserId], [CreatedDate]) VALUES (2015, 2020, 20, 2, CAST(N'2019-07-05' AS Date))
INSERT [dbo].[StockIn] ([Id], [ItemId], [Quantity], [UserId], [CreatedDate]) VALUES (2016, 2021, 5, 2, CAST(N'2019-07-05' AS Date))
INSERT [dbo].[StockIn] ([Id], [ItemId], [Quantity], [UserId], [CreatedDate]) VALUES (2017, 2027, 550, 2, CAST(N'2019-07-05' AS Date))
INSERT [dbo].[StockIn] ([Id], [ItemId], [Quantity], [UserId], [CreatedDate]) VALUES (2018, 2028, 250, 2, CAST(N'2019-07-05' AS Date))
INSERT [dbo].[StockIn] ([Id], [ItemId], [Quantity], [UserId], [CreatedDate]) VALUES (2019, 2022, 30, NULL, CAST(N'2019-06-07' AS Date))
INSERT [dbo].[StockIn] ([Id], [ItemId], [Quantity], [UserId], [CreatedDate]) VALUES (2020, 2022, 20, NULL, CAST(N'2019-06-07' AS Date))
INSERT [dbo].[StockIn] ([Id], [ItemId], [Quantity], [UserId], [CreatedDate]) VALUES (2021, 2022, 100, NULL, CAST(N'2019-06-07' AS Date))
INSERT [dbo].[StockIn] ([Id], [ItemId], [Quantity], [UserId], [CreatedDate]) VALUES (2022, 2022, 6, NULL, CAST(N'2019-06-07' AS Date))
INSERT [dbo].[StockIn] ([Id], [ItemId], [Quantity], [UserId], [CreatedDate]) VALUES (2023, 2033, 45, NULL, CAST(N'2019-06-07' AS Date))
INSERT [dbo].[StockIn] ([Id], [ItemId], [Quantity], [UserId], [CreatedDate]) VALUES (2024, 2033, 5, NULL, CAST(N'2019-06-07' AS Date))
INSERT [dbo].[StockIn] ([Id], [ItemId], [Quantity], [UserId], [CreatedDate]) VALUES (2025, 2033, 10, NULL, CAST(N'2019-06-07' AS Date))
INSERT [dbo].[StockIn] ([Id], [ItemId], [Quantity], [UserId], [CreatedDate]) VALUES (2026, 2021, 15, NULL, CAST(N'2019-06-07' AS Date))
INSERT [dbo].[StockIn] ([Id], [ItemId], [Quantity], [UserId], [CreatedDate]) VALUES (2027, 16, 55, NULL, CAST(N'2019-06-07' AS Date))
INSERT [dbo].[StockIn] ([Id], [ItemId], [Quantity], [UserId], [CreatedDate]) VALUES (2028, 2034, 40, NULL, CAST(N'2019-06-07' AS Date))
INSERT [dbo].[StockIn] ([Id], [ItemId], [Quantity], [UserId], [CreatedDate]) VALUES (2029, 2034, 10, NULL, CAST(N'2019-06-07' AS Date))
INSERT [dbo].[StockIn] ([Id], [ItemId], [Quantity], [UserId], [CreatedDate]) VALUES (2030, 2033, 50, NULL, CAST(N'2019-07-07' AS Date))
INSERT [dbo].[StockIn] ([Id], [ItemId], [Quantity], [UserId], [CreatedDate]) VALUES (2031, 2033, 15, NULL, CAST(N'2019-07-07' AS Date))
SET IDENTITY_INSERT [dbo].[StockIn] OFF
SET IDENTITY_INSERT [dbo].[StockOut] ON 

INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (6, 16, N'Sell', 20, 2, CAST(N'2019-06-24' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (7, 17, N'Sell', 60, 2, CAST(N'2019-06-24' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (8, 1020, N'Lost', 50, 2, CAST(N'2019-06-24' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (9, 1020, N'Damage', 50, 2, CAST(N'2019-06-24' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (10, 1020, N'Damage', 50, 2, CAST(N'2019-06-24' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (11, 14, N'Sell', 50, 2, CAST(N'2019-06-24' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (12, 14, N'Damage', 50, 2, CAST(N'2019-06-24' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (13, 1020, N'Sell', 80, 2, CAST(N'2019-06-24' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (14, 1020, N'Sell', 20, 2, CAST(N'2019-06-24' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (15, 1020, N'Sell', 50, 2, CAST(N'2019-06-24' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (16, 14, N'Sell', 20, 2, CAST(N'2019-06-24' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (17, 16, N'Damage', 5, 2, CAST(N'2019-06-24' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (18, 16, N'Sell', 5, 2, CAST(N'2019-06-24' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (19, 16, N'Sell', 5, 2, CAST(N'2019-06-24' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (20, 16, N'Sell', 5, 2, CAST(N'2019-06-24' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (21, 16, N'Damage', 5, 2, CAST(N'2019-06-24' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (22, 16, N'Sell', 5, 2, CAST(N'2019-06-24' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (23, 16, N'Lost', 5, 2, CAST(N'2019-06-24' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (24, 16, N'Damage', 5, 2, CAST(N'2019-06-24' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (25, 14, N'Sell', 100, 2, CAST(N'2019-06-24' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (26, 14, N'Sell', 170, 2, CAST(N'2019-06-24' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (27, 14, N'Damage', 130, 2, CAST(N'2019-06-24' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (28, 15, N'Sell', 10, 2, CAST(N'2019-06-25' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (29, 18, N'Damage', 5, 2, CAST(N'2019-06-26' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (30, 17, N'Damage', 10, 2, CAST(N'2019-06-26' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (31, 16, N'Lost', 5, 2, CAST(N'2019-06-26' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (32, 2024, N'Sell', 5, 2, CAST(N'2019-06-26' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (33, 2025, N'Sell', 10, 2, CAST(N'2019-06-26' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (34, 2025, N'Sell', 5, 2, CAST(N'2019-06-26' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (35, 2026, N'Sell', 30, 2, CAST(N'2019-07-03' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (36, 2026, N'Sell', 5, 2, CAST(N'2019-07-03' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (37, 2026, N'Sell', 5, 2, CAST(N'2019-07-03' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (38, 2024, N'Lost', 5, 2, CAST(N'2019-07-03' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (39, 2026, N'Sell', 25, 2, CAST(N'2019-07-04' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (40, 2024, N'Sell', 10, 2, CAST(N'2019-07-04' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (41, 2021, N'Lost', 10, 2, CAST(N'2019-07-04' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (42, 2021, N'Lost', 4, 2, CAST(N'2019-07-04' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (43, 2021, N'Lost', 1, 2, CAST(N'2019-07-04' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (44, 2021, N'Sell', 5, 2, CAST(N'2019-07-04' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (45, 2021, N'Sell', 4, 2, CAST(N'2019-07-04' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (46, 2021, N'Sell', 1, 2, CAST(N'2019-07-04' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (47, 2026, N'Sell', 1, 2, CAST(N'2019-07-04' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (48, 2025, N'Sell', 4, 2, CAST(N'2019-07-04' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (49, 2025, N'Sell', 1, 2, CAST(N'2019-07-04' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (50, 2026, N'Sell', 4, 2, CAST(N'2019-07-04' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (51, 2021, N'Damage', 5, 2, CAST(N'2019-07-04' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (52, 2021, N'Lost', 5, 2, CAST(N'2019-07-04' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (53, 1019, N'Sell', 50, 2, CAST(N'2019-07-05' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (54, 2027, N'Sell', 50, 2, CAST(N'2019-07-05' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (55, 2028, N'Sell', 240, 2, CAST(N'2019-07-05' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (56, 2022, N'Sell', 50, 2, CAST(N'2019-07-06' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (57, 2022, N'Damage', 90, 2, CAST(N'2019-07-06' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (58, 2022, N'Damage', 1, 2, CAST(N'2019-07-06' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (59, 2022, N'Damage', 5, 2, CAST(N'2019-07-06' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (60, 2033, N'Sell', 34, 2, CAST(N'2019-07-06' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (61, 2033, N'Sell', 1, 2, CAST(N'2019-07-06' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (62, 16, N'Sell', 14, 2, CAST(N'2019-07-06' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (63, 2033, N'Lost', 5, 2, CAST(N'2019-07-06' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (64, 16, N'Lost', 1, 2, CAST(N'2019-07-06' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (65, 2028, N'Damage', 10, 2, CAST(N'2019-07-06' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (66, 1019, N'Sell', 100, 2, CAST(N'2019-07-06' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (67, 2033, N'Lost', 12, 2, CAST(N'2019-07-06' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (68, 2033, N'Sell', 7, 2, CAST(N'2019-07-06' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (69, 2033, N'Sell', 1, 2, CAST(N'2019-07-06' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (70, 2034, N'Sell', 34, 2, CAST(N'2019-07-07' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (71, 2034, N'Sell', 1, 2, CAST(N'2019-07-07' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (72, 2033, N'Sell', 35, 2, CAST(N'2019-07-07' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (73, 2033, N'Lost', 5, 2, CAST(N'2019-07-07' AS Date))
INSERT [dbo].[StockOut] ([Id], [ItemId], [OutType], [Quantity], [UserId], [CreatedDate]) VALUES (74, 2033, N'Damage', 10, 2, CAST(N'2019-07-07' AS Date))
SET IDENTITY_INSERT [dbo].[StockOut] OFF
SET IDENTITY_INSERT [dbo].[User] ON 

INSERT [dbo].[User] ([Id], [UserName], [Password]) VALUES (1, N'Admin', N'Admin')
INSERT [dbo].[User] ([Id], [UserName], [Password]) VALUES (2, N'Rajib', N'Rajib')
SET IDENTITY_INSERT [dbo].[User] OFF
ALTER TABLE [dbo].[Categories]  WITH CHECK ADD  CONSTRAINT [FK_Categories_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[Categories] CHECK CONSTRAINT [FK_Categories_User]
GO
ALTER TABLE [dbo].[Companys]  WITH CHECK ADD  CONSTRAINT [FK_Companys_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[Companys] CHECK CONSTRAINT [FK_Companys_User]
GO
ALTER TABLE [dbo].[Items]  WITH CHECK ADD  CONSTRAINT [FK_Items_Categories] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[Categories] ([Id])
GO
ALTER TABLE [dbo].[Items] CHECK CONSTRAINT [FK_Items_Categories]
GO
ALTER TABLE [dbo].[Items]  WITH CHECK ADD  CONSTRAINT [FK_Items_Companys] FOREIGN KEY([CompanyId])
REFERENCES [dbo].[Companys] ([Id])
GO
ALTER TABLE [dbo].[Items] CHECK CONSTRAINT [FK_Items_Companys]
GO
ALTER TABLE [dbo].[Items]  WITH CHECK ADD  CONSTRAINT [FK_Items_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[Items] CHECK CONSTRAINT [FK_Items_User]
GO
ALTER TABLE [dbo].[StockIn]  WITH CHECK ADD  CONSTRAINT [FK_StockIn_Items] FOREIGN KEY([ItemId])
REFERENCES [dbo].[Items] ([Id])
GO
ALTER TABLE [dbo].[StockIn] CHECK CONSTRAINT [FK_StockIn_Items]
GO
ALTER TABLE [dbo].[StockIn]  WITH CHECK ADD  CONSTRAINT [FK_StockIn_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[StockIn] CHECK CONSTRAINT [FK_StockIn_User]
GO
ALTER TABLE [dbo].[StockOut]  WITH CHECK ADD  CONSTRAINT [FK_StockOut_Items] FOREIGN KEY([ItemId])
REFERENCES [dbo].[Items] ([Id])
GO
ALTER TABLE [dbo].[StockOut] CHECK CONSTRAINT [FK_StockOut_Items]
GO
ALTER TABLE [dbo].[StockOut]  WITH CHECK ADD  CONSTRAINT [FK_StockOut_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[StockOut] CHECK CONSTRAINT [FK_StockOut_User]
GO
